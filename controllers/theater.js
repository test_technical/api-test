//File: controllers/theater.js
require('../models/movies');
var mongoose = require('mongoose');
var Theaters  = mongoose.model('Theater');

//GET - Return all theaters in the DB
exports.findAllTheaters = function(req, res) {
	Theaters.find()
	.populate('Movies')
	.exec(function(err, theaters) {
    if(err) return res.send(500, err.message);
    console.log(theaters)
    console.log('GET /theaters/');
    	console.log(theaters)
		res.status(200).jsonp(theaters);
	})
};
//GET - Return a theaters with specified ID
exports.findById = function(req, res) {
	Theaters.findById(req.params.id)
	.populate('Movies')
	.exec(function(err, theaters) {
    if(err) return res.send(500, err.message);
    console.log(theaters)
    console.log('GET /theaters/' + req.params.id);
    	console.log(theaters)
		res.status(200).jsonp(theaters);
	})
};

//POST - Insert a new theaters in the DB
exports.addTheater= function(req, res) {
	console.log('POST');
	console.log(req.body);
	var theater = new Theaters({
		name:    	req.body.name,
		address: 	req.body.address,
		movies:  	req.body.movies
	});

	theater.save(function(err, theater) {
		console.log(err)
		if(err) return res.status(500).send( err);
    res.status(200).jsonp(theater);
	});
};

//PUT - Update a register already exists
exports.updateTheater = function(req, res) {
	Theaters.findById(req.params.id, function(err, theater) {
		theater.name   = req.body.name;
		theater.address    = req.body.address;
		theater.movies = req.body.movies;

		theater.save(function(err) {
			if(err) return res.status(500).send(err.message);
      res.status(200).jsonp(theater);
		});
	});
};

//DELETE - Delete a Theaters with specified ID
exports.deleteTheater = function(req, res) {
	Theaters.findById(req.params.id, function(err, theater) {
		theater.remove(function(err) {
			if(err) return res.status(500).send(err.message);
      res.status(200).send();
		})
	});
};

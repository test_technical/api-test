//File: controllers/movies.js
require('../models/movies');
var mongoose = require('mongoose');
var Movies  = mongoose.model('Movies');

//GET - Return all movies in the DB
exports.findAllMovies = function(req, res) {
	Movies.find(function(err, movies) {
    if(err) res.send(500, err.message);

    console.log('GET /movies')
		res.status(200).jsonp(movies);
	});
};
//GET - Return a movies with specified ID
exports.findById = function(req, res) {
	Movies.findById(req.params.id, function(err, movies) {
    if(err) return res.send(500, err.message);

    console.log('GET /movies/' + req.params.id);
		res.status(200).jsonp(movies);
	});
};

//POST - Insert a new movies in the DB
exports.addMovie= function(req, res) {
	console.log('POST');
	console.log(req.body);
	var movie = new Movies({
		title:    req.body.title,
		year: 	  req.body.year,
		lenguaje:  req.body.lenguaje
	});

	movie.save(function(err, movie) {
		if(err) return res.status(500).send( err.message);
    res.status(200).jsonp(movie);
	});
};

//PUT - Update a register already exists
exports.updateMovie = function(req, res) {
	Movies.findById(req.params.id, function(err, movie) {
		movie.title   = req.body.title;
		movie.year    = req.body.year;
		movie.lenguaje = req.body.lenguaje;

		movie.save(function(err) {
			if(err) return res.status(500).send(err.message);
      res.status(200).jsonp(movie);
		});
	});
};

//DELETE - Delete a Movies with specified ID
exports.deleteMovie = function(req, res) {
	Movies.findById(req.params.id, function(err, movie) {
		movie.remove(function(err) {
			if(err) return res.status(500).send(err.message);
      res.status(200).send();
		})
	});
};

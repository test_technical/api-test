var express  = require("express"),
    app      = express(),
    http     = require("http"),
    server   = http.createServer(app),
    bodyParser = require('body-parser'),
    methodOverride  = require("method-override"),
	  mongoose = require('mongoose');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

var router = express.Router();

router.get('/', function(req, res) {
   res.send("Welcome test !");
});

app.use(router);

var MoviesCtrl = require('./controllers/movies');
var TheaterCtrl = require('./controllers/theater');

// API movies
var theaterRoutes = express.Router();
theaterRoutes.route('/theater')
  .get(TheaterCtrl.findAllTheaters)
  .post(TheaterCtrl.addTheater);
theaterRoutes.route('/theater/:id')
  .get(TheaterCtrl.findById)
  .put(TheaterCtrl.updateTheater)
  .delete(TheaterCtrl.deleteTheater);
// API movies
var moviesRoutes = express.Router();
moviesRoutes.route('/movies')
  .get(MoviesCtrl.findAllMovies)
  .post(MoviesCtrl.addMovie);
moviesRoutes.route('/movies/:id')
  .get(MoviesCtrl.findById)
  .put(MoviesCtrl.updateMovie)
  .delete(MoviesCtrl.deleteMovie);

app.use('/api', moviesRoutes);
app.use('/api', theaterRoutes);

mongoose.connect('mongodb://localhost/movies',{
  useMongoClient: true,
  /* other options */
}, function(err, res) {
  if(err) {
    console.log('ERROR: connecting to Database. ' + err);
  }
  app.listen(8080, function() {
    console.log("Node server running on http://localhost:8080");
  });
});
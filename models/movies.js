var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var moviesSchema = new Schema({
  title:    { type: String },
  year:     { type: String },
  lenguaje:  { type: String }
});

module.exports = mongoose.model('Movies', moviesSchema);

var theatersSchema = new Schema({
  name:    { type: String },
  address: { type: String },
  movies: [{ type: Schema.Types.ObjectId, ref: 'Movies' }]
});

module.exports = mongoose.model('Theater', theatersSchema);
